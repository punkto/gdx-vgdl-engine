# LibGDX port of the General Video Game Competition

This code is a port of the game engine (not yet the competition part) used in
the General Video Game Competition (gvgai). This game engine permits develop
small games using a text file for the rules and a set of text files for the
levels. If you want to know more about this amazing framework and competition
please visit the URL [https://github.com/EssexUniversityMCTS/gvgai](https://github.com/EssexUniversityMCTS/gvgai).

Most of the work in this port has been done during the [Ludum Dare 38](http://ludumdare.com/compo/)
competition.

# How to run the demos

You can run a list of the games placed in ./core/assets/examples/gridphysics
following these instructions:

clone this source code and checkout the develop branch

Desktop:
git clone https://github.com/punkto/gdx-vgdl-engine.git
cd gdx-vgdl-engine
git checkout develop
./gradlew desktop:run

HTML:
git clone https://github.com/punkto/gdx-vgdl-engine.git
cd gdx-vgdl-engine
git checkout develop
./gradlew html:dist
cd ./html/build/dist ; python -m SimpleHTTPServer ; cd ../../../

Use your favorite browser and point it to http://0.0.0.0:8000


Please note that the web version is still in development and some (most?) of the
games maybe don't work.

You can alter the order of the list of games, play a specific game or test games
in other directories by editing core/src/eu/mighty/gvgl/screen/IntroScreen.java

# TODO

Fix web version.
